﻿using System.Linq;

namespace GenericPanelManager
{
    public static class AbstractGenericElementExtensions
    {
        public delegate void RunIfMatchDelegate();
        public static void RunIfMatch(this AbstractGenericElement element, RunIfMatchDelegate _runIfMatchDelegate, params AbstractGenericElement[] collection)
        {
            if (collection.ToList().Contains(element))
            {
                _runIfMatchDelegate();
            }
        }

        public static bool IsNotNullAndEqualsTo(this AbstractGenericElement a, AbstractGenericElement b)
        {
            return a != null && a == b;
        }
    }
}
