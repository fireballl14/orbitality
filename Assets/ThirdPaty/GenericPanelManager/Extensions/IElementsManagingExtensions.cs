﻿namespace GenericPanelManager
{
    public static class IElementsManagingExtensions
    {
        public static bool IsAbstractGenericPanelManager(this IElementsManaging manager)
        {
            AbstractGenericPanelManager panelManager = manager as AbstractGenericPanelManager;
            return panelManager != null;
        }

        public static AbstractGenericPanelManager AsAbstractGenericPanelManager(this IElementsManaging manager)
        {
            AbstractGenericPanelManager panelManager = manager as AbstractGenericPanelManager;
            return panelManager;
        }

        public static bool IsClickableManaging(this IElementsManaging manager)
        {
            IClickableManaging clickableManaging = manager as IClickableManaging;
            return clickableManaging != null;
        }

        public static IClickableManaging AsClickableManaging(this IElementsManaging manager)
        {
            IClickableManaging clickableManaging = manager as IClickableManaging;
            return clickableManaging;
        }
    }
}
