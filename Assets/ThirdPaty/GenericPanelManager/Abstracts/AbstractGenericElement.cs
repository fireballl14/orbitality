﻿using System.Collections.Generic;
using UnityEngine;

namespace GenericPanelManager
{
    public abstract class AbstractGenericElement : MonoBehaviour
    {
        public virtual IElementsManaging Manager { get; set; }

        public virtual void Awake()
        {
            RegisterElement();
        }
        
        public virtual void RegisterElement()
        {
            Manager = GetComponentInParent<IElementsManaging>();
            if (Manager == null)
            {
                Debug.LogError("No EscapePanel was found! Elements should always have EscapePanel in parent!", gameObject);
                return;
            }
            if (Manager.RegisteredElements == null)
            {
                Manager.RegisteredElements = new HashSet<AbstractGenericElement>();
            }
            Manager.RegisteredElements.Add(this);
        }
    }
}
