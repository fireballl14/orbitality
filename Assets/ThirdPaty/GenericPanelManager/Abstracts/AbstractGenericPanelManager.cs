﻿using System;
using System.Collections;
using UnityEngine;

namespace GenericPanelManager
{
    public abstract class AbstractGenericPanelManager : MonoBehaviour
    {
		[Tooltip("Panel will disable GameObject on Hide, and enable it on Show.")]
	    public bool ControlGameObjectState = true;
        public enum VisibilityStates
        {
            NotConfigured = -1,
            Appearing = 0,
            Shown = 1,
            Disappearing = 2,
            Hidden = 3,
        }

        public virtual bool IsVisible => _visibilityState == VisibilityStates.Appearing || _visibilityState == VisibilityStates.Shown;

        [SerializeField]private VisibilityStates _visibilityState = VisibilityStates.NotConfigured;
        public virtual VisibilityStates VisibilityState
        {
            get { return _visibilityState; }
            protected set
            {
                if (_visibilityState != value)
                {
                    _visibilityState = value;
                    OnPanelVisibilityStateChanged?.Invoke(this, value);
                    return;
                }
                _visibilityState = value;
            }
        }
        protected VisibilityStates LastIntendentVisibilityState = VisibilityStates.NotConfigured;

        public event EventHandler<VisibilityStates> OnPanelVisibilityStateChanged = null;
        public event EventHandler OnPanelShown = null;
        public event EventHandler OnPanelHide = null;

        public IEnumerator ShowPanelCoroutine { get; private set; }
        public IEnumerator HidePanelCoroutine { get; private set; }

        protected virtual void OnEnable()
        {
	        if (VisibilityState == VisibilityStates.NotConfigured)
	        {
		        Debug.LogError("Please set " + gameObject.name + " panel initial visibility state!");
				return;
	        }
			if (ControlGameObjectState == true)
	        {
		        gameObject.SetActive(IsVisible);
	        }
        }

        [ContextMenu("ShowPanel")]
		public void ShowPanel()
		{
			if (VisibilityState == VisibilityStates.NotConfigured)
	        {
		        VisibilityState = VisibilityStates.Hidden;
			    Debug.LogError("Please set " + gameObject.name + " panel initial visibility state!");
			}
            if (VisibilityState == VisibilityStates.Shown || VisibilityState == VisibilityStates.Appearing)
            {
                return;
            }
            LastIntendentVisibilityState = VisibilityStates.Shown;
            VisibilityState = VisibilityStates.Appearing;
            ShowPanelCoroutine = ShowPanelRoutine();
            if (ControlGameObjectState == true)
            {
	            gameObject.SetActive(true);
            }
            StartCoroutine(ShowPanelCoroutine);
        }

        private IEnumerator ShowPanelRoutine()
        {
            yield return StartCoroutine(ShowPanelImplementation());
            VisibilityState = VisibilityStates.Shown;
            if (OnPanelShown != null)
            {
                OnPanelShown(this, EventArgs.Empty);
            }
            ShowPanelCoroutine = null;
	        if (LastIntendentVisibilityState != VisibilityState)
	        {
		        HidePanel();
	        }
		}

        protected abstract IEnumerator ShowPanelImplementation();

		[ContextMenu("HidePanel")]
		public void HidePanel()
        {
	        if (VisibilityState == VisibilityStates.NotConfigured)
	        {
		        VisibilityState = VisibilityStates.Shown;
		        Debug.LogError("Please set " + gameObject.name + " panel initial visibility state!");
	        }
            if (VisibilityState == VisibilityStates.Hidden || VisibilityState == VisibilityStates.Disappearing)
            {
                return;
            }
            LastIntendentVisibilityState = VisibilityStates.Hidden;
            VisibilityState = VisibilityStates.Disappearing;
            HidePanelCoroutine = HidePanelRoutine();
            StartCoroutine(HidePanelCoroutine);
        }

        private IEnumerator HidePanelRoutine()
        {
            yield return StartCoroutine(HidePanelImplementation());
            VisibilityState = VisibilityStates.Hidden;
            if (OnPanelHide != null)
            {
                OnPanelHide(this, EventArgs.Empty);
            }
            HidePanelCoroutine = null;
            if (LastIntendentVisibilityState != VisibilityState)
	        {
		        ShowPanel();
	        }
			else if (ControlGameObjectState == true)
            {
	            gameObject.SetActive(false);
            }
		}

        protected abstract IEnumerator HidePanelImplementation();
    }
}
