﻿using UnityEngine;
namespace GenericPanelManager
{
    public class GenericClickable : AbstractGenericElement
    {
        public bool RegisterClicksDuringManagerVisibilityTransition = true;
        public bool RegisterClicksWhenHidden = false;

        public virtual void OnClick()
        {
            if (CanNotRegisterClick())
            {
                return;
            }
            Manager.AsClickableManaging().OnClickableWasClicked(this);
        }

        protected virtual bool CanNotRegisterClick()
        {
            return !CanRegisterClick();
        }

        protected virtual bool CanRegisterClick()
        {
            AbstractGenericPanelManager genericPanelManager = Manager as AbstractGenericPanelManager;
            if (!RegisterClicksWhenHidden && null != genericPanelManager && genericPanelManager.VisibilityState == AbstractGenericPanelManager.VisibilityStates.Hidden)
            {
                return false;
            }
            if (!RegisterClicksDuringManagerVisibilityTransition)
            {
                return !ManagerInVisibilityTransitionState();
            }
            return Manager.IsClickableManaging();
        }

        protected virtual bool ManagerInVisibilityTransitionState()
        {
            if (Manager.IsAbstractGenericPanelManager())
            {
                if (Manager.AsAbstractGenericPanelManager().VisibilityState == AbstractGenericPanelManager.VisibilityStates.NotConfigured)
                {
                    Debug.LogWarning("Manager is not typeof Manager.VisibilityState is not configured, all Transition specific checks will be ignored.");
                }
                return Manager.AsAbstractGenericPanelManager().VisibilityState == AbstractGenericPanelManager.VisibilityStates.Appearing ||
                        Manager.AsAbstractGenericPanelManager().VisibilityState == AbstractGenericPanelManager.VisibilityStates.Disappearing;
            }
            Debug.LogWarning("Manager is not typeof AbstractGenericPanelManager");
            return false;
        }
    }
}
