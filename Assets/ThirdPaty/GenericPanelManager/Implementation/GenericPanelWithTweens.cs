﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AnimationOrTween;
using UnityEngine;

namespace GenericPanelManager
{
    [RequireComponent(typeof(UIPlayTween))]
    public class GenericPanelManagerWithTweens : AbstractGenericPanelManager
    {
        protected UIPlayTween TweensController = null;

        protected override IEnumerator ShowPanelImplementation()
        {
            TweensController = GetComponent<UIPlayTween>();
            if (TweensController == null)
            {
                Debug.LogError("TweensController is null! Pls set it up in inspector!", this);
                yield break;
            }
            TweensController.Play(TweensController.playDirection == Direction.Forward);
	        List<UITweener> tweens = GetComponents<UITweener>().Where(tween => tween.enabled && TweensController.tweenGroup == tween.tweenGroup).ToList();
			while (tweens.Any(tween => tween.enabled))
	        {
				yield return null;
			}
        }

        protected override IEnumerator HidePanelImplementation()
        {
            TweensController = GetComponent<UIPlayTween>();
            if (TweensController == null)
            {
                Debug.LogError("TweensController is null! Pls set it up in inspector!", this);
                yield break;
            }
            TweensController.Play(TweensController.playDirection == Direction.Reverse);
	        List<UITweener> tweens = GetComponents<UITweener>().Where(tween => tween.enabled && TweensController.tweenGroup == tween.tweenGroup).ToList();
	        while (tweens.Any(tween => tween.enabled))
	        {
		        yield return null;
	        }
		}
    }
}
