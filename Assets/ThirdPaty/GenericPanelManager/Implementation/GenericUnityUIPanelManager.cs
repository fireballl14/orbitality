﻿using System.Collections;
using UnityEngine;

namespace GenericPanelManager
{
    public class GenericUnityUIPanelManager : AbstractGenericPanelManager
    {
        private CanvasGroup _cachedCanvasGroup;
        protected CanvasGroup CachedCanvasGroup
        {
            set { _cachedCanvasGroup = value; }
            get
            {
                if (_cachedCanvasGroup != null) return _cachedCanvasGroup;
                _cachedCanvasGroup = GetComponentInChildren<CanvasGroup>();
                return _cachedCanvasGroup;
            }
        }

        protected override IEnumerator ShowPanelImplementation()
        {
            if (!gameObject.activeInHierarchy)
            {
                Debug.LogWarning(gameObject.name + " is disabled!");
            }
            CachedCanvasGroup.alpha = 1f;
	        CachedCanvasGroup.blocksRaycasts = true;
	        CachedCanvasGroup.interactable = true;
            yield break;
        }

        protected override IEnumerator HidePanelImplementation()
        {
            if (!gameObject.activeInHierarchy)
            {
                Debug.LogWarning(gameObject.name + " is disabled!");
            }
            CachedCanvasGroup.alpha = 0f;
			CachedCanvasGroup.blocksRaycasts = false;
	        CachedCanvasGroup.interactable = false;
			yield break;
        }
    }
}