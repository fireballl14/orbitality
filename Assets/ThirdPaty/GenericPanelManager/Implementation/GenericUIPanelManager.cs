﻿using System.Collections;
using UnityEngine;

namespace GenericPanelManager
{
    [RequireComponent(typeof(UIPanel))]
    public class GenericUIPanelManager : AbstractGenericPanelManager
    {
        private UIPanel _uiPanel = null;
        protected UIPanel CachedUiPanel
        {
            set { _uiPanel = value; }
            get
            {
                if (_uiPanel != null) return _uiPanel;
                _uiPanel = GetComponent<UIPanel>();
                return _uiPanel;
            }
        }

        protected override IEnumerator ShowPanelImplementation()
        {
            if (!gameObject.activeInHierarchy)
            {
                Debug.LogWarning(gameObject.name +" is disabled!");
            }
            CachedUiPanel.alpha = 1f;
            yield break;
        }

        protected override IEnumerator HidePanelImplementation()
        {
            if (!gameObject.activeInHierarchy)
            {
                Debug.LogWarning(gameObject.name + " is disabled!");
            }
            CachedUiPanel.alpha = 0f;
            yield break;
        }
    }
}
