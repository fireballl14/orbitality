﻿namespace GenericPanelManager
{
    public interface IClickableManaging : IElementsManaging
    {
        void OnClickableWasClicked(GenericClickable clickable);
    }
}
