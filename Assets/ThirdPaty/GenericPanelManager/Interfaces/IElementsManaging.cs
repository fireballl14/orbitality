﻿using System.Collections.Generic;

namespace GenericPanelManager
{
    public interface IElementsManaging
    {
        HashSet<AbstractGenericElement> RegisteredElements { get; set; }
    }
}
