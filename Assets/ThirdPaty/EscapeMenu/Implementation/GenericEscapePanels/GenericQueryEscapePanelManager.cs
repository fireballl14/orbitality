﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using GenericPanelManager;

namespace EscapeMenu
{
    public class GenericQueryEscapePanelManager : GenericPanelManagerWithTweens, IClickableManaging
    {
        private HashSet<AbstractGenericElement> _registeredElements = new HashSet<AbstractGenericElement>();
        public HashSet<AbstractGenericElement> RegisteredElements
        {
            get { return _registeredElements; }
            set { _registeredElements = value; }
        }

	    public bool HidePanelOnClick = true;
	    public UILabel TitleLabel;
		public UILabel TextLabel;
        public UIGrid ButtonsGrid;
        public GenericClickable OkButton;
        public GenericClickable CancelButton;

        [Obsolete("Use ShowPanel(string textKey, OkDelegate okDelegate, CancelDelegate cancelDelegate, bool showOkButton = true, bool showCancelButton = true) instead!", true)]
        public new void ShowPanel(){}

        public delegate void OkPressedDelegate();
        public delegate void CancelPressedDelegate();

	    public OkPressedDelegate OkDelegate = null;
	    public CancelPressedDelegate CancelDelegate = null;
		
	    public virtual void ShowPanel(string text, OkPressedDelegate okPressedDelegate = null,
		    CancelPressedDelegate cancelPressedDelegate = null, bool showOkButton = true, bool showCancelButton = true)
	    {
		    ShowPanel(text, null, okPressedDelegate, cancelPressedDelegate, showOkButton, showCancelButton);
	    }

	    public virtual void ShowPanel(string text, string title, OkPressedDelegate okPressedDelegate, 
		    CancelPressedDelegate cancelPressedDelegate, bool showOkButton = true, bool showCancelButton = true)
        {
	        base.ShowPanel();
            OkDelegate = okPressedDelegate;
            CancelDelegate = cancelPressedDelegate;
            
			if (TitleLabel != null)
			{
				TitleLabel.text = string.IsNullOrEmpty(title) ? "" : title;
			}
			if (TextLabel != null)
			{
				TextLabel.text = string.IsNullOrEmpty(text) ? "" : text;
			}
			NGUITools.SetActive(OkButton.gameObject, showOkButton);
            NGUITools.SetActive(CancelButton.gameObject, showCancelButton);
            if (ButtonsGrid != null)
            {
                ButtonsGrid.repositionNow = true;
            }
        }

        public virtual void OnClickableWasClicked(GenericClickable clickable)
        {
	        if (HidePanelOnClick)
	        {
		        HidePanel();
	        }
			if (OkButton.IsNotNullAndEqualsTo(clickable) && OkDelegate != null)
            {
                OkDelegate();
            }
            else if (CancelDelegate != null)
            {
                CancelDelegate();
            }
        }
    }
}