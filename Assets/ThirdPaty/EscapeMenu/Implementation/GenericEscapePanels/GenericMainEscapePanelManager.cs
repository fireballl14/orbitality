﻿using System;
using System.Collections.Generic;
using GenericPanelManager;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace EscapeMenu
{
    public class GenericMainEscapePanelManager : GenericPanelManagerWithTweens, IClickableManaging
    {
        public HashSet<AbstractGenericElement> RegisteredElements { get; set; } = new HashSet<AbstractGenericElement>();

        public GenericClickable RestartButton;
        public GenericClickable MainMenuButton;
        public GenericClickable QuitButton;

        public GenericQueryEscapePanelManager QueryPanelManager;

        [SerializeField] protected string MainMenuSceneName;

        public virtual void OnClickableWasClicked(GenericClickable clickable)
        {
            if (QueryPanelManager != null)
            {
                if (RestartButton.IsNotNullAndEqualsTo(clickable))
                {
                    QueryPanelManager.OnPanelHide += OnQueryPanelHidePanel;
                    QueryPanelManager.ShowPanel("RestartQuery", OnRestartQueryOkPressed, null);
                }
                if (MainMenuButton.IsNotNullAndEqualsTo(clickable))
                {
                    QueryPanelManager.OnPanelHide += OnQueryPanelHidePanel;
                    QueryPanelManager.ShowPanel("MainMenuQuery", OnMainMenuQueryOkPressed, null);
                }
                if (QuitButton.IsNotNullAndEqualsTo(clickable))
                {
                    QueryPanelManager.OnPanelHide += OnQueryPanelHidePanel;
                    QueryPanelManager.ShowPanel("QuitQuery", OnQuitQueryOkPressed, null);
                }
            }
            HidePanel();
        }

        protected virtual void OnRestartQueryOkPressed()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        protected virtual void OnMainMenuQueryOkPressed()
        {
            SceneManager.LoadScene(MainMenuSceneName);
        }

        protected virtual void OnQuitQueryOkPressed()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            UnityEngine.Application.Quit();
#endif
        }

        protected virtual void OnQueryPanelHidePanel(object sender, EventArgs eventArgs)
        {
	        QueryPanelManager.OnPanelHide -= OnQueryPanelHidePanel;
			ShowPanel();
        }
    }
}