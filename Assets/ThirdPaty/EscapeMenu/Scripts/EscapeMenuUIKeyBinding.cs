using UnityEngine;

public class EscapeMenuUIKeyBinding : MonoBehaviour
{
    [System.Serializable]
	public class Actions
	{
        public bool Click = true;
        public bool Press;
	    public bool Select;
	}
    public Actions Action = new Actions();

    [System.Serializable]
    public class Modifiers
    {
        public bool Shift;
        public bool Control;
        public bool Alt;
    }
    public Modifiers Modifier = new Modifiers();

    public KeyCode KeyCode = KeyCode.None;

    public bool ActivateIfPanelVisible = true;

	private bool IsModifierActive()
    {
        return IsModifierActive(Modifier);
    }
    
	private bool _mPress;
    public virtual void Update()
	{
        bool keyDown = UICamera.GetKeyDown(KeyCode);
		bool keyUp = UICamera.GetKeyUp(KeyCode);
        if (!keyDown && !keyUp) return;
        UICamera uiCamera = UICamera.FindCameraForLayer(gameObject.layer);
        if (uiCamera == null) return;
        if (UICamera.inputHasFocus) return;
		if (KeyCode == KeyCode.None || !IsModifierActive()) return;
        var currentUIPanel = GetComponent<UIPanel>();
        var perentUIPanel = GetComponentInParent<UIPanel>();
        bool panelIsVisible = (currentUIPanel && Mathf.Approximately(currentUIPanel.alpha, 1)) ||
                          (perentUIPanel && Mathf.Approximately(perentUIPanel.alpha, 1));
        #if WINDWARD && UNITY_ANDROID
		// NVIDIA Shield controller has an odd bug where it can open the on-screen keyboard via a KeyCode.Return binding,
		// and then it can never be closed. I am disabling it here until I can track down the cause.
		if (KeyCode == KeyCode.Return && PlayerPrefs.GetInt("Start Chat") == 0) return;
#endif
		if (keyDown) _mPress = true;
        if (panelIsVisible && ActivateIfPanelVisible)
        {
            if (Action.Click || Action.Press)
            {
                if (Action.Press && keyDown)
                {
                    UICamera.currentKey = KeyCode;
                    SendMessage("OnPress", true, SendMessageOptions.DontRequireReceiver);
                }
                if (Action.Click && _mPress && keyUp)
                {
                    UICamera.currentKey = KeyCode;
                    SendMessage("OnPress", false, SendMessageOptions.DontRequireReceiver);
                    SendMessage("OnClick", SendMessageOptions.DontRequireReceiver);
                }
            }
            if (Action.Select)
            {
                if (keyUp)
                {
                    if (!UICamera.inputHasFocus && _mPress)
                    {
                        UICamera.selectedObject = gameObject;
                    }
                    else if (_mPress)
                    {
                        UICamera.hoveredObject = gameObject;
                    }
                }
            }
        }
        if (keyUp)
        {
            _mPress = false;
        }
	}

    static public bool IsModifierActive (Modifiers modifier)
	{
	    if (!modifier.Alt && !modifier.Control && !modifier.Shift)
	    {
	        return !UICamera.GetKey(KeyCode.LeftAlt) && !UICamera.GetKey(KeyCode.RightAlt) &&
	               !UICamera.GetKey(KeyCode.LeftControl) && !UICamera.GetKey(KeyCode.RightControl) &&
	               !UICamera.GetKey(KeyCode.LeftShift) && !UICamera.GetKey(KeyCode.RightShift);
	    }
	    if (modifier.Alt && !modifier.Control && !modifier.Shift)
	    {
	        return UICamera.GetKey(KeyCode.LeftAlt) || UICamera.GetKey(KeyCode.RightAlt);
	    }

	    if (!modifier.Alt && modifier.Control && !modifier.Shift)
	    {
	        return UICamera.GetKey(KeyCode.LeftControl) || UICamera.GetKey(KeyCode.RightControl);
	    }

	    if (!modifier.Alt && !modifier.Control && modifier.Shift)
	    {
	        return UICamera.GetKey(KeyCode.LeftShift) || UICamera.GetKey(KeyCode.RightShift);
	    }
	    if ((modifier.Alt || modifier.Control) && !modifier.Shift)
	    {
	        return (UICamera.GetKey(KeyCode.LeftAlt) || UICamera.GetKey(KeyCode.RightAlt)) &&
	               (UICamera.GetKey(KeyCode.LeftControl) || UICamera.GetKey(KeyCode.RightControl)) &&
	               !UICamera.GetKey(KeyCode.LeftShift) && !UICamera.GetKey(KeyCode.RightShift);
	    }
	    if ((modifier.Alt || modifier.Shift) && !modifier.Control)
	    {
	        return (UICamera.GetKey(KeyCode.LeftAlt) || UICamera.GetKey(KeyCode.RightAlt)) &&
	               !UICamera.GetKey(KeyCode.LeftControl) && !UICamera.GetKey(KeyCode.RightControl) &&
	               (UICamera.GetKey(KeyCode.LeftShift) || UICamera.GetKey(KeyCode.RightShift));
	    }
	    if ((modifier.Control || modifier.Shift) && !modifier.Alt)
	    {
	        return !UICamera.GetKey(KeyCode.LeftAlt) && !UICamera.GetKey(KeyCode.RightAlt) &&
	               (UICamera.GetKey(KeyCode.LeftControl) || UICamera.GetKey(KeyCode.RightControl)) &&
	               (UICamera.GetKey(KeyCode.LeftShift) || UICamera.GetKey(KeyCode.RightShift));
	    }
	    if (modifier.Alt && modifier.Control && modifier.Shift)
	    {
	        return (UICamera.GetKey(KeyCode.LeftAlt) || UICamera.GetKey(KeyCode.RightAlt)) &&
	               (UICamera.GetKey(KeyCode.LeftControl) || UICamera.GetKey(KeyCode.RightControl)) &&
	               (UICamera.GetKey(KeyCode.LeftShift) || UICamera.GetKey(KeyCode.RightShift));
	    }
	    return false;
	}
}
