﻿using LitJsonSrc.delegates;

namespace LitJsonSrc
{
	public interface IJsonSerializableV3 : IJsonSerializableV2
	{
		event Deserialized OnDeserialized;

		bool WasDeserialized { get; }
		int DeserializedVersion { get; }
		SerializatorVersionMigrationType[] VersionsMigrationTable { get; }
		bool IsDeserializationAvailable(int targetVersion);
		//bool IsSerializationAvailable(int targetVersion);
	}
}
