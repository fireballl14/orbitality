﻿namespace LitJsonSrc
{
	/// <summary>
	/// Enumeration of migration types between data versions
	/// </summary>
	public enum SerializatorVersionMigrationType : int
	{
		/// <summary>
		/// Changed just version
		/// </summary>
		Version = 0,
		/// <summary>
		/// Added new fields (not required)
		/// </summary>
		SoftExtension = 1,
		/// <summary>
		/// Added new fields that can affect to existing fields
		/// </summary>
		HardExtension = 2,
		/// <summary>
		/// Added new fields (required)
		/// </summary>
		MixedExtension = 3,
		/// <summary>
		/// Removed fields that was never required
		/// </summary>
		SoftReduction = 4,
		/// <summary>
		/// Removed required fields
		/// </summary>
		HardReduction = 5,
		/// <summary>
		/// Removed fields that was not required only in prev version
		/// </summary>
		MixedReduction = 6,
		/// <summary>
		/// Changed is incompatible
		/// </summary>
		Incompatible = 7,
	}
}