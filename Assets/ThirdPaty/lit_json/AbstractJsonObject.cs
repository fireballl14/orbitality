﻿using LitJsonSrc.delegates;
using System;
using System.Collections;
using System.Collections.Generic;

namespace LitJsonSrc
{
    public abstract class AbstractJsonObject : IJsonSerializableV3, IDisposable
    {
        protected bool m_disposed = false;
        protected const string SERIALIZER_VERSION_KEY = "sv";
        protected const string MIGRATION_TABLE_KEY = "mt";
        private SerializatorVersionMigrationType[] m_baseMigrationTable = null;
        private int m_deserializationNumber = 0;
        protected int m_deserializedVersion = 0;

        public event Deserialized OnDeserialized = null;

        public abstract int SerializatorVersion
        {
            get;
        }

        public virtual bool WasDeserialized
        {
            get
            {
                CheckDisposed();
                return m_deserializationNumber > 0;
            }
            set
            {
                if (value)
                {
                    ++m_deserializationNumber;
                    if (OnDeserialized != null)
                    {
                        OnDeserialized(this, new DeserializedArgs(m_deserializationNumber));
                    }
                }
            }
        }

        public virtual int DeserializedVersion
        {
            get
            {
                CheckDisposed();
                return m_deserializedVersion;
            }
            set
            {
                CheckDisposed();
                m_deserializedVersion = value;
            }
        }

        public virtual SerializatorVersionMigrationType[] VersionsMigrationTable
        {
            get
            {
                CheckDisposed();
                TryToInitBaseMigrationTable();
                return m_baseMigrationTable;
            }
            protected set
            {
                TryToInitBaseMigrationTable();
                m_baseMigrationTable = value;
            }
        }

        public virtual event SerializerVersionOutOfDate OnSerializerVersionOutOfDate = null;

        #region constructors
        public AbstractJsonObject() { }
        public AbstractJsonObject(JsonData serializedSource)
        {
            Deserialize(serializedSource);
        }

        public AbstractJsonObject(string serializedSource)
        {
            Deserialize(serializedSource);
        }

        public AbstractJsonObject(AbstractJsonObject source)
        {
            Deserialize(source.Serialize());
        }
        #endregion

        #region methods
        public abstract bool IsValid(string source, bool deserializeIfValid);
        public abstract bool IsValid(JsonData source, bool deserializeIfValid);

        public virtual void Deserialize(string data)
        {
            CheckDisposed();
            Deserialize(JsonMapper.ToObject(data));
        }

        public virtual void Deserialize(JsonData data)
        {
            CheckDisposed();
            int version = data[SERIALIZER_VERSION_KEY].AsInt();

            if (version <= SerializatorVersion)
            {
                Deserialize(data, version);
                WasDeserialized = true;
                DeserializedVersion = version;
            }
            else
            {
                if (data.ContainsKey(MIGRATION_TABLE_KEY))
                {
                    //read migration table
                    SerializatorVersionMigrationType[] migrationTable = new SerializatorVersionMigrationType[version + 1];
                    for (int i = 0; i <= version; ++i)
                    {
                        migrationTable[i] = (SerializatorVersionMigrationType)data[MIGRATION_TABLE_KEY][i.ToString()].AsInt();
                    }
                    VersionsMigrationTable = migrationTable;
                }

                if (IsDeserializationAvailable(version))
                {
                    Deserialize(data, SerializatorVersion);
                    DeserializedVersion = version;
                }
                else
                {
                    OnSerializerVersionOutOfDateHandler(version);
                }
            }
        }

        public abstract void Deserialize(JsonData data, int version);

        public virtual JsonData Serialize()
        {
            CheckDisposed();
            JsonData result = new JsonData();
            result[SERIALIZER_VERSION_KEY] = SerializatorVersion;
            JsonData migrationTable = new JsonData();
            SerializatorVersionMigrationType[] originalTable = VersionsMigrationTable;
            for (int i = 0; i < originalTable.Length; ++i)
            {
                migrationTable[i.ToString()] = (int)originalTable[i];
            }
            result[MIGRATION_TABLE_KEY] = migrationTable;
            return result;
        }

        public virtual string ToJson()
        {
            CheckDisposed();
            return Serialize().ToJson();
        }

        public virtual string ToPrettyJson()
        {
            CheckDisposed();
            return Serialize().ToPrettyJson();
        }


        protected virtual void OnSerializerVersionOutOfDateHandler(int requiredVersion)
        {
            if (OnSerializerVersionOutOfDate != null)
            {
                OnSerializerVersionOutOfDate(GetType(), requiredVersion);
            }
        }
        public virtual bool IsDeserializationAvailable(int targetVersion)
        {
            CheckDisposed();
            bool result = targetVersion == VersionsMigrationTable.Length - 1;
            for (int i = VersionsMigrationTable.Length - 1; i > SerializatorVersion && result; --i)
            {
                SerializatorVersionMigrationType t = VersionsMigrationTable[i];
                switch (t)
                {
                    case SerializatorVersionMigrationType.HardExtension:
                    case SerializatorVersionMigrationType.HardReduction:
                    case SerializatorVersionMigrationType.MixedReduction: result = targetVersion - SerializatorVersion == 1; break;
                    case SerializatorVersionMigrationType.Incompatible: result = false; break;
                }
            }
            return result;
        }



        private void TryToInitBaseMigrationTable()
        {
            if (m_baseMigrationTable == null)
            {
                m_baseMigrationTable = new SerializatorVersionMigrationType[SerializatorVersion + 1];
                m_baseMigrationTable[0] = SerializatorVersionMigrationType.Version;
                for (int i = 1; i < m_baseMigrationTable.Length; ++i)
                {
                    m_baseMigrationTable[i] = SerializatorVersionMigrationType.Incompatible;
                }
            }
        }

        public virtual void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (m_disposed) return;
            if (disposing)
            {
                m_baseMigrationTable = null;
                m_disposed = true;
            }
        }

        protected virtual void CheckDisposed()
        {
            if (m_disposed) throw new ObjectDisposedException(GetType().Name);
        }

        public static JsonData Serialize(object[] data)
        {
            JsonData result = new JsonData();
            result.SetJsonType(JsonType.Array);
            for (int i = 0; i < data.Length; ++i)
            {
                result.Add(data[i]);
            }
            return result;
        }

        public static JsonData Serialize(IEnumerable data)
        {
            JsonData result = new JsonData();
            result.SetJsonType(JsonType.Array);
            IEnumerator e = data.GetEnumerator();
            while (e.MoveNext())
            {
                result.Add(e.Current);
            }
            return result;
        }

        public static JsonData Serialize<T>(IEnumerable<T> data) where T : IJsonSerializable
        {
            JsonData result = new JsonData();
            result.SetJsonType(JsonType.Array);
            using (IEnumerator<T> e = data.GetEnumerator())
            {
                while (e.MoveNext())
                {
                    result.Add(e.Current.Serialize());
                }
            }
            return result;
        }

        public static void Deserialize<T>(JsonData data, out List<T> result) where T : IJsonSerializable, new()
        {
            result = new List<T>(data.Count);
            for (int i = 0; i < data.Count; ++i)
            {
                T item = new T();
                item.Deserialize(data[i]);
                result.Add(item);
            }
        }

        public static void Deserialize<T>(JsonData data, out T[] result) where T : IJsonSerializable, new()
        {
            result = new T[data.Count];
            for (int i = 0; i < data.Count; ++i)
            {
                T item = new T();
                item.Deserialize(data[i]);
                result[i] = item;
            }
        }

        public static void Deserialize(JsonData data, out string[] result)
        {
            result = new string[data.Count];
            for (int i = 0; i < data.Count; ++i)
                result[i] = data[i].AsString();

        }

        public static void Deserialize(JsonData data, out int[] result)
        {
            result = new int[data.Count];
            for (int i = 0; i < data.Count; ++i)
                result[i] = data[i].AsInt();

        }

        public static void Deserialize(JsonData data, out double[] result)
        {
            result = new double[data.Count];
            for (int i = 0; i < data.Count; ++i)
                result[i] = data[i].AsDouble();

        }

        public static void Deserialize(JsonData data, out List<string> result)
        {
            result = new List<string>(data.Count);
            for (int i = 0; i < data.Count; ++i)
            {
                result.Add(data[i].AsString());
            }
        }

        public static void Deserialize(JsonData data, out List<int> result)
        {
            result = new List<int>(data.Count);
            for (int i = 0; i < data.Count; ++i)
                result.Add(data[i].AsInt());
        }

        public static void Deserialize(JsonData data, out List<bool> result)
        {
            result = new List<bool>(data.Count);
            for (int i = 0; i < data.Count; ++i)
                result.Add(data[i].AsBool());
        }

        public static void Deserialize(JsonData data, out List<double> result)
        {
            result = new List<double>(data.Count);
            for (int i = 0; i < data.Count; ++i)
                result.Add(data[i].AsDouble());
        }
        #endregion

        //public virtual bool IsSerializationAvailable(int targetVersion)
        //{
        //	bool result = true;

        //		for (int i = VersionsMigrationTable.Length - 1; i > SerializatorVersion && result; --i)
        //		{
        //			SerializatorVersionMigrationType t = VersionsMigrationTable[i];
        //			switch (t)
        //			{
        //				case SerializatorVersionMigrationType.HardExtension:
        //				case SerializatorVersionMigrationType.MixedExtension:
        //				case SerializatorVersionMigrationType.Incompatible: result = false; break;
        //			}
        //		}


        //	return result;
        //}
    }
}