﻿namespace LitJsonSrc
{
	public delegate void SerializerVersionOutOfDate(System.Type type, int requireVersion);
	public interface IJsonSerializableV2 : IJsonSerializable
	{
		/// <summary>
		/// first parameter - type of the interface realization
		/// second parameter - target serializer version to deserialize content
		/// </summary>
		event SerializerVersionOutOfDate OnSerializerVersionOutOfDate;
	}
}