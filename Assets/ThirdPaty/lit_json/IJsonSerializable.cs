﻿namespace LitJsonSrc
{
	/// <summary>
	/// Serialization and migration between formats interface
	/// </summary>
	public interface IJsonSerializable
	{
		#region methods
		/// <summary>
		/// Converts internal object view into JsonData object
		/// </summary>
		/// <returns>JsonData object</returns>
		JsonData Serialize();
		/// <summary>
		/// Converts internal object view into string
		/// </summary>
		/// <returns>String view</returns>
		string ToJson();
		/// <summary>
		/// Read JonData object to restore internal object view
		/// </summary>
		/// <param name="data">Serialized object</param>
		void Deserialize(JsonData data);
		/// <summary>
		/// Read string object representation to restore internal object view
		/// </summary>
		/// <param name="data">Serialized to string object</param>
		void Deserialize(string data);
		#endregion
	}
}