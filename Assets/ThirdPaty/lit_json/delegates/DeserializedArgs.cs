﻿using System;

namespace LitJsonSrc.delegates
{
	public delegate void Deserialized(object sender, DeserializedArgs e);

	public sealed class DeserializedArgs : EventArgs
	{
		public int Number
		{
			get;
			private set;
		}

		public DeserializedArgs(int number)
		{
			Number = number;
		}
	}
}
