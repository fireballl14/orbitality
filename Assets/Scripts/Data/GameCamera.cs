﻿using UnityEngine;

public class GameCamera : MonoBehaviour
{
	public static GameCamera Instance;
	public Camera Camera;

	private void Awake()
	{
		if (Instance != null)
		{
			Debug.LogError("There is more then one instance of '" + nameof(GameCamera) + "' singleton! This is not allowed!", this);
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#endif
			return;
		}
		Instance = this;
		if (Camera == null)
		{
			Camera = GetComponent<Camera>();
		}
	}

}
