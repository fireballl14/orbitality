﻿using System;
using System.Collections.Generic;
using System.Linq;
using LitJsonSrc;
using UnityEngine;

public class Player : IJsonSerializable
{
	public readonly List<string> AiNames = new List<string> { "Lucas", "Anita", "Sonnie", "Mitch", "Shania", "Eileen", "Leighton", "Mamie", "Kev", "Duncan" };

	private Guid _guid = Guid.NewGuid();
	public Guid Guid => _guid;
	public string Name;
	public PlanetType PlanetType;
	public ControllerType ControllerType;
	public Planet PlanetInstance;
	public JsonData PlanetData = null;

	public Player GenerateRandomAi()
	{
		List<Planet> planets = new List<Planet>(GameDataBase.Instance.Planets);
		Planet randomPlanet = planets.OrderBy(x => Guid.NewGuid()).Take(1).Single();
		List<string> names = new List<string>(AiNames);
		string randomName = names.OrderBy(x => Guid.NewGuid()).Take(1).Single();
		return new Player {PlanetType = randomPlanet.Type, Name = randomName, ControllerType = ControllerType.Ai};
	}

	#region Serialization
	public JsonData Serialize()
	{
		JsonData data = new JsonData();
		data["Sv"] = SerializatorVersion;
		data["Guid"] = Guid.ToString("N");
		data["Name"] = Name;
		data["ControllerType"] = (int)ControllerType;
		data["PlanetType"] = (int)PlanetType;
		data["PlanetData"] = PlanetInstance.Serialize();
		return data;
	}

	public string ToJson()
	{
		return Serialize().ToJson();
	}

	public void Deserialize(JsonData data)
	{
		int serializatorVersion = data["Sv"].AsInt();
		Deserialize(data, serializatorVersion);
	}

	public int SerializatorVersion => 0;



	public void Deserialize(string data)
	{
		Deserialize(JsonMapper.ToObject(data));
	}

	public void Deserialize(JsonData data, int version)
	{
		switch (version)
		{
			case 0:
				DeserializeV0(data);
				break;
			default:
				Debug.LogError("Invalid SerializatorVersion, version '" + version + "' is not supported!");
				break;
		}
	}

	private void DeserializeV0(JsonData data)
	{
		_guid = new Guid(data["Guid"].AsString());
		Name = data["Name"].AsString();
		PlanetType = (PlanetType)data["PlanetType"].AsInt();
		ControllerType = (ControllerType)data["ControllerType"].AsInt();
		PlanetData = data["PlanetData"];
	}
	#endregion
}