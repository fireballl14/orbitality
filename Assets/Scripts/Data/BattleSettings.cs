﻿using System.Collections.Generic;
using LitJsonSrc;
using UnityEngine;

public class BattleSettings : IJsonSerializable
{
	public int MinOrbit;
	public int MaxOrbit;
	public List<Player> Players;
	public JsonData ProjectilesJsonData;

	#region Serialization
	public JsonData Serialize()
	{
		JsonData data = new JsonData();
		data["Sv"] = SerializatorVersion;
		data["MaxOrbit"] = MaxOrbit;
		data["MinOrbit"] = MinOrbit;
		JsonData playersJsonData = new JsonData();
		playersJsonData.SetJsonType(JsonType.Array);
		foreach (Player playerSetting in Players)
		{
			playersJsonData.Add(playerSetting.Serialize());
		}
		data["Player"] = playersJsonData;
		if (BattleManager.ActiveProjectiles.Count > 0)
		{
			JsonData projectilesJsonData = new JsonData();
			projectilesJsonData.SetJsonType(JsonType.Array);
			foreach (Projectile projectile in BattleManager.ActiveProjectiles)
			{
				projectilesJsonData.Add(projectile.Serialize());
			}
			data["Projectiles"] = projectilesJsonData;
		}
		return data;
	}

	public string ToJson()
	{
		return Serialize().ToJson();
	}

	public void Deserialize(JsonData data)
	{
		int serializatorVersion = data["Sv"].AsInt();
		Deserialize(data, serializatorVersion);
	}

	public int SerializatorVersion => 0;

	public void Deserialize(string data)
	{
		Deserialize(JsonMapper.ToObject(data));
	}

	public void Deserialize(JsonData data, int version)
	{
		switch (version)
		{
			case 0:
				DeserializeV0(data);
				break;
			default:
				Debug.LogError("Invalid SerializatorVersion, version '" + version +
				               "' is not supported!");
				break;
		}
	}

	private void DeserializeV0(JsonData data)
	{
		MaxOrbit = data["MaxOrbit"].AsInt();
		MinOrbit = data["MinOrbit"].AsInt();
		Players = new List<Player>();
		foreach (JsonData playerSettingJsonData in data["Player"])
		{
			Player player = new Player();
			player.Deserialize(playerSettingJsonData);
			Players.Add(player);
		}
		if (data.ContainsKey("Projectiles"))
		{
			ProjectilesJsonData = data["Projectiles"];
		}
	}
	#endregion
}