﻿using System.Collections.Generic;
using UnityEngine;

public class GameDataBase : MonoBehaviour
{
	public static GameDataBase Instance;

	public PlanetUi PlanetUiPrefab = null;
	public List<Projectile> Projectiles = new List<Projectile>();
	public List<Planet> Planets = new List<Planet>();
	public List<GameObject> Stars = new List<GameObject>();

	private void Awake()
	{
		if (Instance != null)
		{
			Debug.LogError("There is more then one instance of '" + nameof(GameDataBase) + "' singleton! This is not allowed!", this);
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#endif
			return;
		}
		Instance = this;
	}

	public GameObject GetRandomStar()
	{
		return Stars[Random.Range(0, Stars.Count - 1)];
	}

	public Planet GetPlanetByType(PlanetType type)
	{
		return Planets.Find(planet => planet.Type == type);
	}

	public Projectile GetProjectileByType(ProjectileType type)
	{
		return Projectiles.Find(projectile => projectile.Type == type);
	}
}