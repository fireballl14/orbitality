﻿using System;
using System.Globalization;
using LitJsonSrc;
using UnityEngine;

public class Planet : MonoBehaviour
{
	public event EventHandler<int> OnHit;

	public PlanetType Type = PlanetType.Earth;
	[Tooltip("1 = slow, 2 = normal, 3 = very fast, 10+ = instant")]
	[Range(0, 10)] public float RotationSpeed = 2f;
	public int Hp = 1;
	public int HpMax = 1;
	public Weapon Weapon;

	public Player Owner { get; private set; }

	private PlanetUi _planetUi;

	public void Initialize(Player player)
	{
		Owner = player;
		gameObject.name = Owner.Name;
		Orbiter orbiter = GetComponent<Orbiter>();
		orbiter.SetupOrbiter(BattleManager.StarInstance.transform);
		switch (Owner.ControllerType)
		{
			case ControllerType.Human:
				AddPlayerController();
				break;
			case ControllerType.Ai:
				AddAiManager();
				break;
		}
		AddPlanetUi();
		if (Owner.PlanetData != null)
		{
			Deserialize(Owner.PlanetData);
		}
	}

	private void AddAiManager()
	{
		AiManager aiManager = gameObject.AddComponent<AiManager>();
		aiManager.RotationSpeed = RotationSpeed;
	}

	private void AddPlayerController()
	{
		PlayerController playerController = gameObject.AddComponent<PlayerController>();
		playerController.RotationSpeed = RotationSpeed;
	}

	private void AddPlanetUi()
	{
		GameObject planetHolder = new GameObject(gameObject.name + "Holder");
		transform.SetParent(planetHolder.transform);
		_planetUi = Instantiate(GameDataBase.Instance.PlanetUiPrefab, transform.root);
		_planetUi.Planet = this;
	}

	public void Hit(int damage)
	{
		Hp -= damage;
		OnHit?.Invoke(this, Hp);
		if (Hp <= 0)
		{
			Die();
		}
	}

	private void Die()
	{
		BattleManager.CurrentSettings.Players.Remove(Owner);
		if (Owner.ControllerType == ControllerType.Human || BattleManager.CurrentSettings.Players.Count == 1)
		{
			BattleManager.EndBattle();
		}
		Destroy(transform.root.gameObject);
	}

	#region Serialization
	public JsonData Serialize()
	{
		JsonData data = new JsonData();
		data["Sv"] = SerializatorVersion;
		data["OwnerGuid"] = Owner.Guid.ToString("N");
		data["Hp"] = Hp;
		Orbiter orbiter = GetComponent<Orbiter>();
		data["Speed"] = orbiter.CurrentSpeed.ToString(CultureInfo.InvariantCulture);
		data["Direction"] = (int)orbiter.Direction;
		Vector3 transformPosition = transform.localPosition;
		data["PosX"] = transformPosition.x.ToString(CultureInfo.InvariantCulture);
		data["PosY"] = transformPosition.y.ToString(CultureInfo.InvariantCulture);
		data["PosZ"] = transformPosition.z.ToString(CultureInfo.InvariantCulture);
		Vector3 transformRotation = transform.localRotation.eulerAngles;
		data["RotX"] = transformRotation.x.ToString(CultureInfo.InvariantCulture);
		data["RotY"] = transformRotation.y.ToString(CultureInfo.InvariantCulture);
		data["RotZ"] = transformRotation.z.ToString(CultureInfo.InvariantCulture);
		return data;
	}

	public string ToJson()
	{
		return Serialize().ToJson();
	}

	public void Deserialize(JsonData data)
	{
		int serializatorVersion = data["Sv"].AsInt();
		Deserialize(data, serializatorVersion);
	}

	public int SerializatorVersion => 0;

	public void Deserialize(string data)
	{
		Deserialize(JsonMapper.ToObject(data));
	}

	public void Deserialize(JsonData data, int version)
	{
		switch (version)
		{
			case 0:
				DeserializeV0(data);
				break;
			default:
				Debug.LogError("Invalid SerializatorVersion, version '" + version + "' is not supported!");
				break;
		}
	}

	private void DeserializeV0(JsonData data)
	{
		Owner = BattleManager.CurrentSettings.Players.Find(player => player.Guid == new Guid(data["OwnerGuid"].AsString()));
		Hp = data["Hp"].AsInt();
		Orbiter orbiter = GetComponent<Orbiter>();
		orbiter.Direction = (MovementDirection)data["Direction"].AsInt();
		orbiter.CurrentSpeed = float.Parse(data["Speed"].AsString(), CultureInfo.InvariantCulture);
		Vector3 position = new Vector3();
		position.x = float.Parse(data["PosX"].AsString(), CultureInfo.InvariantCulture);
		position.y = float.Parse(data["PosY"].AsString(), CultureInfo.InvariantCulture);
		position.z = float.Parse(data["PosZ"].AsString(), CultureInfo.InvariantCulture);
		Vector3 rotation = new Vector3();
		rotation.x = float.Parse(data["RotX"].AsString(), CultureInfo.InvariantCulture);
		rotation.y = float.Parse(data["RotY"].AsString(), CultureInfo.InvariantCulture);
		rotation.z = float.Parse(data["RotZ"].AsString(), CultureInfo.InvariantCulture);
		transform.localPosition = position;
		transform.localRotation = Quaternion.Euler(rotation);
	}
	#endregion
}