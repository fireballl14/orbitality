﻿using System.Globalization;
using LitJsonSrc;
using UnityEngine;

public enum ProjectileType
{
	Light = 1,
	Medium = 2,
	Heavy = 3,
}

public class Projectile : MonoBehaviour, IJsonSerializable
{
	public ProjectileType Type = ProjectileType.Light;
	public int Damage = 1;
	public Vector3 InitialSpeed = Vector3.forward;
	public float LifeTime = 5f;

	private void Start()
	{
		GetComponent<GravityObject>().SetSpeed(InitialSpeed, Space.Self);
	}

	private void FixedUpdate()
	{
		LifeTime -= Time.fixedDeltaTime;
		if (LifeTime <= 0)
		{
			Destroy(transform.root.gameObject);
		}
		Rigidbody rigidbody = GetComponent<Rigidbody>();
		RotateToVelocity(rigidbody, Mathf.Infinity);
	}

	private void RotateToVelocity(Rigidbody rigidbody, float turnSpeed)
	{
		Vector3 direction = new Vector3(rigidbody.velocity.x, 0f, rigidbody.velocity.z);
		if (direction.magnitude > 0.1)
		{
			Quaternion dirQ = Quaternion.LookRotation(direction);
			Quaternion slerp = Quaternion.Slerp(transform.rotation, dirQ, direction.magnitude * turnSpeed * Time.deltaTime);
			rigidbody.MoveRotation(slerp);
		}
	}

	private void OnDestroy()
	{
		BattleManager.ActiveProjectiles.Remove(this);
	}

	private void OnCollisionEnter(Collision other)
	{
		Planet planet = other.gameObject.GetComponent<Planet>();
		Projectile projectile = other.gameObject.GetComponent<Projectile>();
		if (planet != null)
		{
			planet.Hit(Damage);
			Destroy(gameObject);
		}
		else if (projectile != null)
		{
			Destroy(other.gameObject);
			Destroy(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
	}

	#region Serialization
	public JsonData Serialize()
	{
		JsonData data = new JsonData();
		data["Sv"] = SerializatorVersion;
		data["Type"] = (int)Type;
		data["LifeTime"] = LifeTime.ToString(CultureInfo.InvariantCulture);
		Vector3 velocity = GetComponent<Rigidbody>().velocity;
		data["VelX"] = velocity.x.ToString(CultureInfo.InvariantCulture);
		data["VelY"] = velocity.y.ToString(CultureInfo.InvariantCulture);
		data["VelZ"] = velocity.z.ToString(CultureInfo.InvariantCulture);
		Vector3 transformPosition = transform.localPosition;
		data["PosX"] = transformPosition.x.ToString(CultureInfo.InvariantCulture);
		data["PosY"] = transformPosition.y.ToString(CultureInfo.InvariantCulture);
		data["PosZ"] = transformPosition.z.ToString(CultureInfo.InvariantCulture);
		Vector3 transformRotation = transform.localRotation.eulerAngles;
		data["RotX"] = transformRotation.x.ToString(CultureInfo.InvariantCulture);
		data["RotY"] = transformRotation.y.ToString(CultureInfo.InvariantCulture);
		data["RotZ"] = transformRotation.z.ToString(CultureInfo.InvariantCulture);
		return data;
	}

	public string ToJson()
	{
		return Serialize().ToJson();
	}

	public void Deserialize(JsonData data)
	{
		int serializatorVersion = data["Sv"].AsInt();
		Deserialize(data, serializatorVersion);
	}

	public int SerializatorVersion => 0;



	public void Deserialize(string data)
	{
		Deserialize(JsonMapper.ToObject(data));
	}

	public void Deserialize(JsonData data, int version)
	{
		switch (version)
		{
			case 0:
				DeserializeV0(data);
				break;
			default:
				Debug.LogError("Invalid SerializatorVersion, version '" + version + "' is not supported!");
				break;
		}
	}

	private void DeserializeV0(JsonData data)
	{
		LifeTime = float.Parse(data["LifeTime"].AsString(), CultureInfo.InvariantCulture);
		Vector3 velocity = new Vector3();
		velocity.x = float.Parse(data["VelX"].AsString(), CultureInfo.InvariantCulture);
		velocity.y = float.Parse(data["VelY"].AsString(), CultureInfo.InvariantCulture);
		velocity.z = float.Parse(data["VelZ"].AsString(), CultureInfo.InvariantCulture);
		Vector3 position = new Vector3();
		position.x = float.Parse(data["PosX"].AsString(), CultureInfo.InvariantCulture);
		position.y = float.Parse(data["PosY"].AsString(), CultureInfo.InvariantCulture);
		position.z = float.Parse(data["PosZ"].AsString(), CultureInfo.InvariantCulture);
		Vector3 rotation = new Vector3();
		rotation.x = float.Parse(data["RotX"].AsString(), CultureInfo.InvariantCulture);
		rotation.y = float.Parse(data["RotY"].AsString(), CultureInfo.InvariantCulture);
		rotation.z = float.Parse(data["RotZ"].AsString(), CultureInfo.InvariantCulture);
		transform.localPosition = position;
		transform.localRotation = Quaternion.Euler(rotation);
		GetComponent<Rigidbody>().velocity = velocity;
	}
	#endregion
}