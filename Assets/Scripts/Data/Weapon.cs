﻿using UnityEngine;

public class Weapon : MonoBehaviour
{
	public Projectile ProjectilePrefab;
	public Transform Muzzle;
	public float Cooldown;
	
	private float _lastShootTime;

	public void TryToShoot()
	{
		if (_lastShootTime + Cooldown > Time.fixedTime)
		{
			return;
		}
		SpawnProjectile(ProjectilePrefab);
		_lastShootTime = Time.fixedTime;
	}

	private void SpawnProjectile(Projectile projectilePrefab)
	{
		Vector3 muzzlePosition = Muzzle.position;
		Projectile projectile = Instantiate(projectilePrefab, muzzlePosition, Quaternion.LookRotation(muzzlePosition - transform.position, Vector3.up));
		projectile.InitialSpeed += new Vector3(0,0, GetComponentInParent<Orbiter>().CurrentSpeed);
		BattleManager.ActiveProjectiles.Add(projectile);
	}
}