﻿using UnityEngine;

public static class TransformExtensions
{
	public static void RotateTowards(this Transform transform, Vector3 target, float rotationSpeed)
	{
		if (rotationSpeed >= 10)
		{
			transform.LookAt(target);
		}
		else
		{
			Vector3 targetDir = target - transform.position;
			Vector3 forward = transform.forward;
			Quaternion rotation = Quaternion.RotateTowards(Quaternion.LookRotation(forward), Quaternion.LookRotation(targetDir),rotationSpeed);
			transform.rotation = rotation;
		}
	}
}