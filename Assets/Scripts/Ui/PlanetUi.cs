﻿using TMPro;
using UnityEngine;

public class PlanetUi : MonoBehaviour
{
	public Billboard Billboard;
	public TextMeshPro NameLabel;
	public HpBar HpBar;

	private Planet _planet;
	public Planet Planet
	{
		get => _planet;
		set
		{
			_planet = value;
			NameLabel.text = Planet.transform.name;
			HpBar.Planet = _planet;
			Billboard.Planet = _planet;
		}
	}
}