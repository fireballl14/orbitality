﻿using System;
using System.Collections;
using System.Collections.Generic;
using GenericPanelManager;
using UnityEngine;

public class MainMenuPanel : GenericPanelManagerWithTweens, IClickableManaging
{
	public static MainMenuPanel Instance;

	public GenericClickable ToBattleClickable;
	public GenericClickable LoadGameClickable;
	public GenericClickable QuitClickable;

	private void Awake()
	{
		if (Instance != null)
		{
			Debug.LogError("There is more then one instance of '" + nameof(MainMenuPanel) + "' singleton! This is not allowed!", this);
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#endif
			return;
		}
		Instance = this;
	}

	public HashSet<AbstractGenericElement> RegisteredElements { get; set; }
	public void OnClickableWasClicked(GenericClickable clickable)
	{
		if (QuitClickable.IsNotNullAndEqualsTo(clickable))
		{
			HidePanel();
			SingletonQueryPanel.Instance.OnPanelHide += OnQueryPanelHidePanel;
			SingletonQueryPanel.Instance.ShowPanel("Are you sure you want to quit?", OnQuitQueryOkPressed, null);
		}
		if (LoadGameClickable.IsNotNullAndEqualsTo(clickable))
		{
			bool loadedSuccessfully = SaveManager.Load();
			if (loadedSuccessfully == true)
			{
				HidePanel();
				InGamePanel.Instance.ShowPanel();
				SingletonQueryPanel.Instance.ShowPanel("Game loaded!", ResumeGameGame, null, true, false);
				GameDataBase.Instance.StartCoroutine(PauseGame());
			}
		}
		if (ToBattleClickable.IsNotNullAndEqualsTo(clickable))
		{
			GameSettingsPanel.Instance.ShowPanel();
			HidePanel();
		}
	}

	private void OnQueryPanelHidePanel(object sender, EventArgs e)
	{
		SingletonQueryPanel.Instance.OnPanelHide -= OnQueryPanelHidePanel;
		ShowPanel();
	}

	private void OnQuitQueryOkPressed()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
            UnityEngine.Application.Quit();
#endif
	}

	private IEnumerator PauseGame()
	{
		//Waiting for UIGrid on SingletonQueryPanel to do its job.
		yield return null;
		yield return null;
		yield return null;

		BattleManager.PauseGame();
	}

	private void ResumeGameGame()
	{
		BattleManager.ResumeGame();
	}
}