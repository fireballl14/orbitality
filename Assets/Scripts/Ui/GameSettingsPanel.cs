﻿using System;
using System.Collections;
using System.Collections.Generic;
using GenericPanelManager;
using UnityEngine;

public class GameSettingsPanel : GenericPanelManagerWithTweens, IClickableManaging
{
	public static GameSettingsPanel Instance;

	public int MinOrbit = 3;
	public int MaxOrbit = 15;

	public UIInput PlayerNameInput;
	public UILabel OpponentsCounterLabel;

	public GenericClickable RandomClickable;
	public GenericClickable EarthClickable;
	public GenericClickable MarsClickable;
	public GenericClickable VenusClickable;
	public GenericClickable LessOpponentsClickable;
	public GenericClickable MoreOpponentsClickable;
	public GenericClickable StartClickable;
	public GenericClickable CloseClickable;

	private PlanetType _selectedPlanetType;

	private int _opponentsCount = 1;
	private int OpponentsCount
	{
		get
		{
			return _opponentsCount;
		}
		set
		{
			_opponentsCount = value;
			_opponentsCount = Mathf.Clamp(OpponentsCount, 1, MaxOrbit - MinOrbit - 1);
		}
	}

	private void Awake()
	{
		if (Instance != null)
		{
			Debug.LogError("There is more then one instance of '" + nameof(GameSettingsPanel) + "' singleton! This is not allowed!", this);
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#endif
			return;
		}
		Instance = this;
	}

	protected override IEnumerator ShowPanelImplementation()
	{
		_selectedPlanetType = GetRandomPlanetType();
		RandomClickable.GetComponent<UIToggle>().Set(true, true);
		OpponentsCounterLabel.text = OpponentsCount.ToString();
		return base.ShowPanelImplementation();
	}
	
	public HashSet<AbstractGenericElement> RegisteredElements { get; set; }
	public void OnClickableWasClicked(GenericClickable clickable)
	{
		if (RandomClickable.IsNotNullAndEqualsTo(clickable))
		{
			_selectedPlanetType = GetRandomPlanetType();
		}
		if (EarthClickable.IsNotNullAndEqualsTo(clickable))
		{
			_selectedPlanetType = PlanetType.Earth;
		}
		if (MarsClickable.IsNotNullAndEqualsTo(clickable))
		{
			_selectedPlanetType = PlanetType.Mars;
		}
		if (VenusClickable.IsNotNullAndEqualsTo(clickable))
		{
			_selectedPlanetType = PlanetType.Venus;
		}
		if (LessOpponentsClickable.IsNotNullAndEqualsTo(clickable))
		{
			OpponentsCount--;
			OpponentsCounterLabel.text = OpponentsCount.ToString();
		}
		if (MoreOpponentsClickable.IsNotNullAndEqualsTo(clickable))
		{
			OpponentsCount++;
			OpponentsCounterLabel.text = OpponentsCount.ToString();
		}
		if (StartClickable.IsNotNullAndEqualsTo(clickable))
		{
			HidePanel();
			InGamePanel.Instance.ShowPanel();
			BattleSettings settings = GenerateBattleSettings();
			BattleManager.StartGame(settings);
		}
		if (CloseClickable.IsNotNullAndEqualsTo(clickable))
		{
			HidePanel();
			MainMenuPanel.Instance.ShowPanel();
		}
	}

	private static PlanetType GetRandomPlanetType()
	{
		Array values = Enum.GetValues(typeof(PlanetType));
		System.Random random = new System.Random();
		PlanetType randomPlanetType = (PlanetType) values.GetValue(random.Next(values.Length));
		return randomPlanetType;
	}

	private BattleSettings GenerateBattleSettings()
	{
		List<Player> players = new List<Player>();
		players.Add(new Player
		{
			Name = PlayerNameInput.value == PlayerNameInput.defaultText ? "Player" : PlayerNameInput.value,
			PlanetType = _selectedPlanetType,
			ControllerType = ControllerType.Human
		});
		for (int i = 0; i < OpponentsCount; i++)
		{
			players.Add(new Player().GenerateRandomAi());
		}
		BattleSettings settings = new BattleSettings();
		settings.MaxOrbit = MaxOrbit;
		settings.MinOrbit = MinOrbit;
		settings.Players = players;
		return settings;
	}
}