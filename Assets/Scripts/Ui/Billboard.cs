﻿using UnityEngine;

public class Billboard : MonoBehaviour
{
	public Planet Planet;
	private Vector3 _initialPosition;

	private void Awake()
	{
		_initialPosition = transform.position;
	}

	private void FixedUpdate()
    {
	    if (Planet == null)
	    {
			return;
	    }
		transform.forward = GameCamera.Instance.transform.forward;
		transform.position = Planet.transform.position + _initialPosition;
    }
}
