﻿using UnityEngine;

public class AbstractUnitBarController : MonoBehaviour
{
    private float _barPercentage = 100f;
    public float BarPercentage
    {
        set
        {
            _barPercentage = value;
            Vector3 localScale = transform.localScale;
            float value01 = Mathf.Clamp(value / 100f, 0f, 1f);
            localScale = new Vector3(value01, localScale.y);
            transform.localScale = localScale;
        }
        get => _barPercentage;
    }
}