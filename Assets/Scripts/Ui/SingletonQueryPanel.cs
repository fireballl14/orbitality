﻿using EscapeMenu;
using UnityEngine;

public class SingletonQueryPanel : GenericQueryEscapePanelManager
{
	public static SingletonQueryPanel Instance;

	private void Awake()
	{
		if (Instance != null)
		{
			Debug.LogError("There is more then one instance of '" + nameof(SingletonQueryPanel) + "' singleton! This is not allowed!", this);
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#endif
			return;
		}
		Instance = this;
	}
}