﻿using System.Collections;
using System.Collections.Generic;
using GenericPanelManager;
using UnityEngine;

public class VictoryPanel : GenericPanelManagerWithTweens, IClickableManaging
{
	public static VictoryPanel Instance;

	private void Awake()
	{
		if (Instance != null)
		{
			Debug.LogError("There is more then one instance of '" + nameof(VictoryPanel) + "' singleton! This is not allowed!", this);
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#endif
			return;
		}
		Instance = this;
	}

	protected override IEnumerator ShowPanelImplementation()
	{
		InGamePanel.Instance.HidePanel();
		return base.ShowPanelImplementation();
	}

	public HashSet<AbstractGenericElement> RegisteredElements { get; set; }
    public void OnClickableWasClicked(GenericClickable clickable)
    {
	    HidePanel();
		MainMenuPanel.Instance.ShowPanel();
    }
}
