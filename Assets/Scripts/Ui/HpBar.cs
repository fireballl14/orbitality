﻿public class HpBar : AbstractUnitBarController
{
	private Planet _planet = null;
	public Planet Planet
    {
	    get { return _planet; }
	    set
	    {
		    _planet = value;
		    gameObject.SetActive(true);
	    }
    }

    private void OnEnable()
    {
		if (Planet == null)
        {
            gameObject.SetActive(false);
            return;
        }
        Planet.OnHit += OnCurrentValueChanged;
        UpdateBarPercentage(Planet.Hp, Planet.HpMax);
    }

    private void UpdateBarPercentage(int hpCurrent, int hpMax)
    {
	    BarPercentage = (float)hpCurrent / hpMax * 100f;
    }

    private void OnDisable()
    {
	    if (Planet != null)
	    {
		    Planet.OnHit -= OnCurrentValueChanged;
	    }
    }

    protected virtual void OnCurrentValueChanged(object sender, int hp)
    {
	    UpdateBarPercentage(hp,((Planet)sender).HpMax);
    }
}