﻿using System.Collections;
using System.Collections.Generic;
using GenericPanelManager;
using UnityEngine;

public class InGamePanel : GenericPanelManagerWithTweens, IClickableManaging
{
	public static InGamePanel Instance;

	public GenericClickable PauseClickable;
	public GenericClickable SaveClickable;
	public GenericClickable LoadClickable;
	public GenericClickable GiveUpClickable;
	
	private void Awake()
	{
		if (Instance != null)
		{
			Debug.LogError("There is more then one instance of '" + nameof(InGamePanel) + "' singleton! This is not allowed!", this);
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#endif
			return;
		}
		Instance = this;
	}
	
	public HashSet<AbstractGenericElement> RegisteredElements { get; set; }
	public void OnClickableWasClicked(GenericClickable clickable)
	{
		if (LoadClickable.IsNotNullAndEqualsTo(clickable))
		{
			SaveManager.Load();
			SingletonQueryPanel.Instance.ShowPanel("Game loaded!", ResumeGameGame, null, true, false);
			HidePanel();
			GameDataBase.Instance.StartCoroutine(PauseGame());
		}
		if (SaveClickable.IsNotNullAndEqualsTo(clickable))
		{
			SaveManager.Save();
			SingletonQueryPanel.Instance.ShowPanel("Game saved!", ResumeGameGame, null, true, false);
			HidePanel();
			GameDataBase.Instance.StartCoroutine(PauseGame());
		}
		if (PauseClickable.IsNotNullAndEqualsTo(clickable))
		{
			SingletonQueryPanel.Instance.ShowPanel("Game paused", ResumeGameGame, null, true, false);
			HidePanel();
			GameDataBase.Instance.StartCoroutine(PauseGame());
		}
		if (GiveUpClickable.IsNotNullAndEqualsTo(clickable))
		{
			BattleManager.DoDefeat();
		}
	}

	private IEnumerator PauseGame()
	{
		//Waiting for UIGrid on SingletonQueryPanel to do its job.
		yield return null;
		yield return null;
		yield return null;

		BattleManager.PauseGame();
	}

	private void ResumeGameGame()
	{
		ShowPanel();
		BattleManager.ResumeGame();
	}
}