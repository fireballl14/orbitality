﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
	[Tooltip("1 = slow, 2 = normal, 3 = very fast, 10+ = instant")]
	[Range(0, 10)] public float RotationSpeed = 2f;
	private Planet _playersPlanet;

	private void Start()
	{
		_playersPlanet = GetComponent<Planet>();
	}

	private void FixedUpdate()
	{
		if (GameCamera.Instance == null)
		{
			Debug.LogError("No " + nameof(GameCamera) + " found on scene!", this);
			return;
		}
		Vector3 mousePosition = GameCamera.Instance.Camera.ScreenToWorldPoint(Input.mousePosition);
		mousePosition.y = 0;
		transform.RotateTowards(mousePosition, RotationSpeed);
		if (Input.GetMouseButton(0))
		{
			_playersPlanet.Weapon.TryToShoot();
		}
	}


}