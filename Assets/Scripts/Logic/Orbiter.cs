﻿using UnityEngine;

public class Orbiter : MonoBehaviour
{
	public Transform Center;
	public float MinSpeed;
	public float MaxSpeed;
	public float CurrentSpeed;
	public MovementDirection Direction = MovementDirection.Clockwise;
	
	private void Start()
	{
		if (Center == null)
		{
			Debug.LogError(nameof(Center) + " is not set! Please set it in " + nameof(Orbiter) + " of '"+ gameObject.name +"' game object!", this);
		}
	}

	private void FixedUpdate()
	{
		if (Mathf.Approximately(CurrentSpeed ,0f))
		{
			return;
		}
		RotateAround(Center, new Vector3(0,CurrentSpeed,0));
	}

	private void RotateAround(Transform center, Vector3 speed)
	{
		transform.RotateAround(center.position, Direction == MovementDirection.Clockwise ? speed.normalized : -speed.normalized, speed.magnitude);
	}

	public void SetRandomSpeedAndDirection(float minSpeed, float maxSpeed)
	{
		Direction = (MovementDirection)Random.Range(1, 3);
		CurrentSpeed = Random.Range(minSpeed, maxSpeed);
	}

	public void SetupOrbiter(Transform center)
	{
		Center = BattleManager.StarInstance.transform;
		SetRandomSpeedAndDirection(MinSpeed, MaxSpeed);
	}
}