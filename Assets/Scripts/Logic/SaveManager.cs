﻿using UnityEngine;

public static class SaveManager
{
	private const string SAVE_KEY = "23379D10-204F-4BF2-929D-AB784DF52CBE";

	public static bool Load()
	{
		if (PlayerPrefs.HasKey(SAVE_KEY) == false)
		{
			SingletonQueryPanel.Instance.ShowPanel("No saved game found!");
			Debug.LogWarning("No save game found!");
			return false;
		}
		string jsonString = PlayerPrefs.GetString(SAVE_KEY);
		BattleSettings settings = new BattleSettings();
		settings.Deserialize(jsonString);
		BattleManager.StartGame(settings);
		return true;
	}

	public static string Save()
	{
		string battleSettingsJson = BattleManager.CurrentSettings.ToJson();
		PlayerPrefs.SetString(SAVE_KEY, battleSettingsJson);
		PlayerPrefs.Save();
		return battleSettingsJson;
	}
}