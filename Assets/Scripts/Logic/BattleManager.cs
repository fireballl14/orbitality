﻿using System;
using System.Collections.Generic;
using System.Linq;
using LitJsonSrc;
using UnityEngine;
using Object = UnityEngine.Object;

public static class BattleManager
{
	public static List<Projectile> ActiveProjectiles = new List<Projectile>();
	public static GameObject StarInstance;
	public static BattleSettings CurrentSettings { get; private set; } = null;

	public static void PauseGame()
	{
		Time.timeScale = 0f;
	}

	public static void ResumeGame()
	{
		Time.timeScale = 1f;
	}

	public static void StartGame(BattleSettings settings)
	{
		CleanUpScene();
		CurrentSettings = settings;
		StarInstance = Object.Instantiate(GameDataBase.Instance.GetRandomStar());
		foreach (Player player in CurrentSettings.Players)
		{
			CreatePlanetForPlayer(player);
		}
		if (settings.ProjectilesJsonData != null)
		{
			DeserializeProjectilesJsonData(settings);
		}
	}
	
	public static void EndBattle()
	{
		if (CurrentSettings.Players.Any(player => player.ControllerType == ControllerType.Human))
		{
			DoVictory();
		}
		else
		{
			DoDefeat();
		}
	}

	public static void DoVictory()
	{
		VictoryPanel.Instance.ShowPanel();
		CleanUpScene();
	}

	public static void DoDefeat()
	{
		DefeatPanel.Instance.ShowPanel();
		CleanUpScene();
	}

	public static void CleanUpScene()
	{
		if (CurrentSettings != null)
		{
			foreach (Player player in CurrentSettings.Players)
			{
				Object.Destroy(player.PlanetInstance.transform.root.gameObject);
			}
		}
		foreach (Projectile activeProjectile in ActiveProjectiles)
		{
			Object.Destroy(activeProjectile.transform.root.gameObject);
		}
		Object.Destroy(StarInstance);
		CurrentSettings = null;
	}

	private static Planet CreatePlanetForPlayer(Player player)
	{
		int playerOrbit = SelectRandomFreeOrbit(CurrentSettings.MinOrbit, CurrentSettings.MaxOrbit,
			GetOccupiedOrbits(CurrentSettings.Players), CurrentSettings.Players.Count);
		Planet playerPlanet = Object.Instantiate(GameDataBase.Instance.GetPlanetByType(player.PlanetType),
			new Vector3(playerOrbit, 0, 0), Quaternion.identity);
		player.PlanetInstance = playerPlanet;
		playerPlanet.Initialize(player);
		return playerPlanet;
	}

	private static void DeserializeProjectilesJsonData(BattleSettings settings)
	{
		foreach (JsonData projectileData in settings.ProjectilesJsonData)
		{
			ProjectileType projectileType = (ProjectileType)projectileData["Type"].AsInt();
			Projectile projectile = Object.Instantiate(GameDataBase.Instance.GetProjectileByType(projectileType));
			projectile.Deserialize(projectileData);
		}
	}

	private static int SelectRandomFreeOrbit(int minOrbit, int maxOrbit, List<int> occupiedOrbits, int playersCount)
	{
		if (occupiedOrbits.Count >= playersCount)
		{
			Debug.LogError("There are more players than available orbits!");
			return -1;
		}
		List<int> freeOrbits = GetFreeOrbits(minOrbit, maxOrbit, occupiedOrbits);
		return freeOrbits.OrderBy(x => Guid.NewGuid()).Take(1).Single();
	}

	private static List<int> GetOccupiedOrbits(List<Player> players)
	{
		List<int> occupiedOrbits = new List<int>();
		foreach (Player player in players)
		{
			if (player.PlanetInstance == null)
			{
				continue;
			}
			occupiedOrbits.Add(Mathf.RoundToInt(player.PlanetInstance.transform.position.x));
		}
		return occupiedOrbits;
	}

	private static List<int> GetFreeOrbits(int minOrbit, int maxOrbit, List<int> occupiedOrbits)
	{
		List<int> freeOrbits = new List<int>();
		for (int i = minOrbit; i < maxOrbit; i++)
		{
			if (occupiedOrbits.Contains(i) == false)
			{
				freeOrbits.Add(i);
			}
		}
		return freeOrbits;
	}
}