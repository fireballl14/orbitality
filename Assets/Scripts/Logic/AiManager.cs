﻿using System.Collections.Generic;
using UnityEngine;

public class AiManager : MonoBehaviour
{
	[Tooltip("1 = slow, 2 = normal, 3 = very fast, 10+ = instant")]
	[Range(0, 10)] public float RotationSpeed = 2f;
	public Transform Target;
	public float RetargetingInterval = 2f;
	public float ShootingAngleThreshold = 25;

	private float _lastRetargetingTime = -1;
	private Planet _planet;

	private void Awake()
	{
		_planet = GetComponent<Planet>();
		_lastRetargetingTime = Time.fixedTime + RetargetingInterval;
	}

	public void FixedUpdate()
	{
		if (Target == null || _lastRetargetingTime + RetargetingInterval < Time.fixedTime) 
		{
			Target = FindClosestPlanet();
		}
		if (Target == null)
		{
			return;
		}
		transform.RotateTowards(Target.position, RotationSpeed);
		if (IsLookingAtTarget(Target) == true)
		{
			_planet.Weapon.TryToShoot();
		}
	}

	private bool IsLookingAtTarget(Transform target)
	{
		Vector3 targetDir = target.position - transform.position;
		Vector3 forward = transform.forward;
		float angle = Vector3.SignedAngle(targetDir, forward, Vector3.up);
		return Mathf.Abs(angle) < ShootingAngleThreshold;
	}

	private Transform FindClosestPlanet()
	{
		KeyValuePair<Planet, float>  closestPlanet = new KeyValuePair<Planet, float>(null,Mathf.Infinity);
		foreach (Player player in BattleManager.CurrentSettings.Players)
		{
			if (player.Guid == _planet.Owner.Guid || player.PlanetInstance == null)
			{
				continue;
			}
			float distance = Vector3.Distance(transform.position, player.PlanetInstance.transform.position);
			if (distance < closestPlanet.Value)
			{
				closestPlanet = new KeyValuePair<Planet, float>(player.PlanetInstance, distance);
			}
		}
		_lastRetargetingTime = Time.fixedTime;
		return closestPlanet.Key.transform;
	}
}
