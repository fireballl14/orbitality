﻿using System.Collections.Generic;
using UnityEngine;

public class GravityObject : MonoBehaviour
{
	public static readonly List<GravityObject> GRAVITY_OBJECTS_LIST = new List<GravityObject>();

	public LayerMask AttractedTo;

	private Rigidbody _rigidbody = null;
	public Rigidbody Rigidbody
	{
		get
		{
			if (_rigidbody == null)
			{
				_rigidbody = GetComponent<Rigidbody>();
			}
			return _rigidbody;
		}
	}

	private void OnEnable()
	{
		AddObjectToGravityObjectsList(this);
	}
	
	private void OnDisable()
	{
		RemoveObjectFromGravityObjectsList(this);
	}

	public void SetSpeed(Vector3 speed, Space space)
	{
		if (space == Space.World)
		{
			Rigidbody.velocity = speed;
		}
		else if (space == Space.Self)
		{
			Rigidbody.velocity = transform.TransformDirection(speed);
		}
	}

	private static void AddObjectToGravityObjectsList(GravityObject gravityObject)
	{
		GRAVITY_OBJECTS_LIST.Add(gravityObject);
	}

	private static void RemoveObjectFromGravityObjectsList(GravityObject gravityObject)
	{
		bool hasBeenRemoved = GRAVITY_OBJECTS_LIST.Remove(gravityObject);
		if (hasBeenRemoved == false)
		{
			Debug.LogWarning("No "+ nameof(GravityObject) +" with name '" + gravityObject.gameObject.name 
			                 + "' has been found in "+ nameof(GRAVITY_OBJECTS_LIST) + "!", gravityObject);
		}
	}

	private void FixedUpdate()
	{
		Vector3 position = Rigidbody.position;
		Vector3 acceleration = Vector3.zero;
		foreach(GravityObject gravityObject in GRAVITY_OBJECTS_LIST)
		{
			bool isInteractableObject = (AttractedTo & 1 << gravityObject.gameObject.layer) != 0;
			if (gravityObject == this || isInteractableObject == false)
			{
				continue;
			}
			Vector3 direction = (gravityObject.Rigidbody.position - position);
			acceleration += direction.normalized * gravityObject.Rigidbody.mass / direction.sqrMagnitude;
		}
		Vector3 thisFrameAcceleration = acceleration * Time.fixedDeltaTime / Rigidbody.mass;

		//On loading game from save acceleration can become NaN for one frame;
		if (float.IsNaN(thisFrameAcceleration.x) || float.IsNaN(thisFrameAcceleration.y) || float.IsNaN(thisFrameAcceleration.z))
		{
			return;
		}
		Rigidbody.velocity += thisFrameAcceleration;
	}
}
